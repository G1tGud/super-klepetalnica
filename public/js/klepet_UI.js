/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  //ZA SLIKE
  var spo = sporocilo.split(" ");
  var s = "";
  var bes1 = "";
  var bes2 = "";
  for(var i=0; i<spo.length; i++) {
    if(spo[i].includes("http")) {
      s = spo[i];
      for(var j=i+1; j<spo.length; j++) {
        bes2 = bes2 + " " + spo[j];
      }
    break;
    }
    else {
      bes1 = bes1 + spo[i] + " ";
    }
  }
  if(s.startsWith("http") && (s.endsWith(".jpg") || s.endsWith(".png") || s.endsWith(".gif"))) {
     return $('<div style="font-weight: bold"></div>').html(bes1 + '<br /><img src=' + s + ' alt="Napaka" style="width:200px; padding-left:20px;">' + bes2 + '<br />');
  }
  //BESEDILO
  else {
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

var tabelaVzdevkov = [[],[]];
var st = 0;
/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if(sistemskoSporocilo.ime && sistemskoSporocilo.vzdevek) {
      var nasel = false;
      for(var i = 0; i<tabelaVzdevkov[0].length; i++) {
        if(tabelaVzdevkov[i][0] == sistemskoSporocilo.ime) {
          tabelaVzdevkov[i][1] = sistemskoSporocilo.vzdevek;
          nasel = true;
          break;
        }
      }
      if(nasel == false) {
        if(!tabelaVzdevkov[st]) {
          tabelaVzdevkov[st] = [];
        }
        tabelaVzdevkov[st][0] = sistemskoSporocilo.ime;
        tabelaVzdevkov[st][1] = sistemskoSporocilo.vzdevek;
        st++;
      }
    }
    else {
      if (sistemskoSporocilo) {
        if(sistemskoSporocilo.includes("html" && (".jpg" || ".png" || ".gif"))) {
          $('#sporocila').append(divElementEnostavniTekst(sistemskoSporocilo));
        }
        else {
         $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
        }
      }
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var novElement = "";
    if(sporocilo.spremembaVzdevka) {
      for(var i=0; i<tabelaVzdevkov[0].length; i++) {
        if(sporocilo.starVzdevek == tabelaVzdevkov[i][0]) {
          tabelaVzdevkov[i][0] = sporocilo.novVzdevek;
          break;
        }
      }
    }
    
    if(sporocilo.vzdevek && sporocilo.tekst) {
      var spremeniVzdevek = sporocilo.vzdevek;
      for(var i=0; i<tabelaVzdevkov[0].length; i++) {
        if(tabelaVzdevkov[i][0] == spremeniVzdevek) {
          spremeniVzdevek = tabelaVzdevkov[i][1] + " (" + tabelaVzdevkov[i][0] + ")";
          break;
        }
      }
      novElement = spremeniVzdevek + sporocilo.tekst;
      $('#sporocila').append(divElementEnostavniTekst(novElement));
    }
    else {
      novElement = divElementEnostavniTekst(sporocilo.besedilo);
      $('#sporocila').append(divElementEnostavniTekst(sporocilo.besedilo));
    }
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();
    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    
    for (var i=0; i < uporabniki.length; i++) {
      var novUporabnik = uporabniki[i];
      for(var j = 0; j<tabelaVzdevkov[0].length; j++) {
        if(novUporabnik == tabelaVzdevkov[j][0]) {
          novUporabnik = tabelaVzdevkov[j][1] + " (" + tabelaVzdevkov[j][0] +")";
          break;
        }
      }
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(novUporabnik));
    }
    
    // Klik na ime prejemnika v seznamu udeležencev pošlje POKE
    $('#seznam-uporabnikov div').click(function() {
      var sporocilo = klepetApp.procesirajUkaz('/zasebno ' + '"' + $(this).text() + '"' +' "☜');
      $('#sporocila').append(divElementHtmlTekst(sporocilo));
      $('#poslji-sporocilo').focus();
    });
  });
  
  
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
  
});


 